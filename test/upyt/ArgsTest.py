# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


import upyt.unittest as ut
import upyt.args as args

class ArgsTest(ut.UnitTest):

    def testEmptyInit(self):
        a = args.args()

    @ut.ExpectException(SyntaxError)
    def testIllegalOpt(self):
        args.args(opts='abcABC1', kws={})


    def testOpts(self):
        a = args.args(opts='Lk')
        a.parse(argv=[])
        self.assertFalse(a.L)
        self.assertFalse(a.k)

        a = args.args(opts='Lk')
        a.parse(argv=['-k'])
        self.assertFalse(a.L)
        self.assertTrue(a.k)

        a = args.args(opts='Lk')
        a.parse(argv=['-k', '-L'])
        self.assertTrue(a.L)
        self.assertTrue(a.k)

        a = args.args(opts='Lk')
        a.parse(argv=['-kL'])
        self.assertTrue(a.L)
        self.assertTrue(a.k)

    def testKeyValuePairs(self):
        a = args.args(opts='', kws={'one':int,'two':bool})
        a.parse(argv=[])
        self.assertNone(a.one)
        self.assertNone(a.two)

        a = args.args(opts='', kws={'one':int,'two':bool})
        a.parse(argv=['--one:33', '--two:True'])
        self.assertEquals(33, a.one)
        self.assertEquals(True, a.two)

    def testKeyValuePairsWithDefaults(self):
        a = args.args(opts='',
                      kws={'one':str,'two':float},
                      defaults={'one':'Roger','two':42.42})
        a.parse(argv=[])
        self.assertEquals('Roger', a.one)
        self.assertEquals(42.42, a.two)

        a = args.args(opts='',
                      kws={'one':str,'two':float},
                      defaults={'one':'Roger','two':42.42})
        a.parse(argv=['--one:Hans', '--two:666.0'])
        self.assertEquals('Hans', a.one)
        self.assertEquals(666.0, a.two)

    def testMixedStuff(self):
        a = args.args(opts='abc',
                      kws={'one':str,'two':float,'three':bool,'four':int},
                      defaults={'one':'Roger','four':42})
        a.parse(['-a', '--one:one', '--three:Yes', '-c'])

        self.assertTrue(a.a)
        self.assertFalse(a.b)
        self.assertTrue(a.c)

        self.assertEquals('one', a.one)
        self.assertNone(a.two)
        self.assertTrue(a.three)
        self.assertEquals(42, a.four)


    @ut.ExpectException(args.ArgSyntaxError)
    def testEmptyKey(self):
        a = args.args(opts='', kws={'one':str})
        a.parse(['--one'])
