# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


import inspect
import re
import logging
import numpy as np
        
class AssertException(Exception):
    def __init__(self, msg):
        super(AssertException, self).__init__(msg)

class AssertError(Exception):
    def __init__(self, msg):
        super(AssertError, self).__init__(msg)

class GoalError(AssertException):
    def __init__(self,test):
        super(GoalError,self).__init__('Failed to achieve goal ' + str(test))
        self.test = test

class GoalAchieved(Exception):
    def __init__(self,test):
        super(GoalAchieved,self).__init__('Achieved goal!!! -- ' + str(test))
        self.test = test


class ExpectException:
    '''
    Annotation for tests that expect to fail with an exception.
    '''
    def __init__(self, exClass):
        self._exClass = exClass
        self._log     = logging.getLogger(__name__ + '.' + self.__class__.__name__)
        
    def __call__(self, testFunc):
        expectedEx = self._exClass
        def unittest_ExpectException_call(*args):
            succeeded = True
            try:
                self._log.debug('Calling function with expected exception: ' + str(testFunc))
                testFunc(*args)
                succeeded = False
            except expectedEx:
                pass
            
            if not succeeded:
                raise AssertException('Exception of type ' + self._exClass.__name__ + ' expected.')
            
        result           = unittest_ExpectException_call
        result.__name__  = testFunc.__name__
        return result

class Goal:
    '''
    Annotation for tests that are goals -- i.e. tests that are not expected to work at
    the moment, but should work in the near future.
    '''
    def __init__(self,*args):
        self._log = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def __call__(self, testFunc):
        self._log.debug('Annotating function ' + str(testFunc))
        def unittest_GoalCall(*args):
            succeeded = True
            try:
                self._log.debug('Running function ' + str(testFunc))
                testFunc(*args)                
                raise GoalAchieved(testFunc)
            except AssertException as ass:
                raise GoalError(testFunc)

        result = unittest_GoalCall
        result.__name__ = testFunc.__name__
        return result
    
class Asserts:

    ARRAY_DISPLAY_LIMIT = 7

    def __init__(self):
        self.isStrictOnBool = False
    
    def setStrictOnBool(self):
        self.isStrictOnBool = True
    
    def fail(self, msg=''):
        raise AssertException(msg)
    
    def assertTrue(self, expr, msg=''):
        self._assertIsBoolValueable(expr)           
        
        if not expr:
            raise AssertException(msg)

    def assertAllTrue(self, expr, msg=''):
        if not np.all(expr):
            if len(expr) <= Asserts.ARRAY_DISPLAY_LIMIT:
                raise AssertException(msg + ' was: <' + str(expr) + '>')
            else:
                raise AssertException(msg)

    def assertFalse(self, expr, msg=''):
        self._assertIsBoolValueable(expr)
        
        if expr:
            raise AssertException(msg)

    def assertAllFalse(self, expr, msg=''):
        nexpr = [not e for e in expr]
        if not np.all(nexpr):
            if len(expr) <= Asserts.ARRAY_DISPLAY_LIMIT:
                raise AssertException(msg + ' was: <' + str(expr) + '>')
            else:
                raise AssertException(msg)
        
    def assertNotNone(self, expr, msg=''):
        if expr is None:
            raise AssertException(msg)

    def assertNone(self, expr, msg=''):
        if expr is not None:
            raise AssertException(msg)
        
    def assertEquals(self, expected, actual, msg=''):
        if not expected == actual:
            if len(msg) > 0:
                msg = msg + ' '
            raise AssertException(msg + 'Expected:<' + str(expected) + \
                                  '>, was: <' + str(actual) + '>.')

    def assertAllEquals(self, expected, actual, msg=''):
        if len(msg) > 0:
            msg = msg + ' '
        if not (len(expected) == len(actual)):
            raise AssertException(msg + 'Lengths differ, Expected:<' + str(len(expected)) +
                                  ', was:<' + str(len(actual)) + '>')
        if not np.all(np.equal(expected,actual)):
            if max(len(actual),len(expected)) <= Asserts.ARRAY_DISPLAY_LIMIT:
                raise AssertException(msg + 'Expected:<' + str(expected) + \
                                      '>, was: <' + str(actual) + '>.')
            else:
                raise AssertException(msg + ' Arrays differ.')

    def assertClose(self, expected, actual, threshold=1e-10, msg=''):
        diff = abs(expected - actual)
        if len(msg) > 0:
            msg = msg + ' '
        if diff > threshold:
            raise AssertException(msg + 'Expected:<' + str(expected) + \
                                  '>, was: <' + str(actual) + '>. Maximal threshold: <' + \
                                  str(threshold) + '>, actual difference: <' + str(diff) + '>.')

            
    def assertAllClose(self, expected, actual, threshold=1e-10, msg=''):
        if len(msg) > 0:
            msg = msg + ' '
        if not (len(expected) == len(actual)):
            raise AssertException(msg + 'Lengths differ, Expected:<' + str(len(expected)) +
                                  '>, was:<' + str(len(actual)) + '>')
        diff = [abs(a-b) for a,b in zip(expected, actual)]
        cmp  = [d < threshold for d in diff]
        if not np.all(cmp):
            if max(len(actual),len(expected)) <= Asserts.ARRAY_DISPLAY_LIMIT:
                txt = '%s Expected:<%s>, was:<%s>, max difference:%f' % \
                      (msg, str(expected), str(actual), max(diff))
                raise AssertException(txt)
            else:
                raise AssertException(msg + ' Arrays differ (Max difference: ' \
                                      + str(max(diff)) + ')')

        
        if not (len(expected) == len(actual) and np.all(expected-actual <= threshold)):
            if len(msg) > 0:
                msg = msg + ' '
            if max(len(actual),len(expected)) <= 5:
                raise AssertException(msg + 'Expected:<' + str(expected) + \
                                                 '>, was: <' + str(actual) + '>.')
            else:
                raise AssertException(msg + ' Arrays are not close enough.')

    def _assertIsBoolValueable(self, obj):
        if self.isStrictOnBool:
            if not np.any((isinstance(obj, bool),
                           isinstance(obj, int),
                           isinstance(obj, np.bool_),
                           hasattr(obj, '__nonzero__'))):
                raise AssertError('Expression does not valuate to a boolean expression. Was ' + \
                                  str(type(obj)))


            
            
class UnitTest(Asserts):
    
    def __init__(self):
        Asserts.__init__(self)
        self._log = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def getTestMethods(self):
        attrs  = [getattr(self, f) for f in dir(self)]
        self._log.debug('atr: ' + str(attrs))
        self._log.debug('dir: ' + str(dir(self)))
        tests  = [a for a in attrs if self._isTestMethod(a)]
        return tests
        
    def _isTestMethod(self, m):

        self._log.debug('Checking attribute ' + str(m))

        if not inspect.ismethod(m):
            return False
        
        # Name must start with 'test' and continue as a camel
        # cased name, or with an underscore
        if not re.match(r'^test[_A-Z0-9].*', m.__name__):
            return False

        # It must be an instance method with no arguments except self
        spec = inspect.getargspec(m)
        if (len(spec.args) == 1 and spec.args[0] is not 'self') or len(spec.args) > 1:
            print('WARNING: Non-test method with test method name prefix: ', m, spec)
            return False

        return True



class MethodMocker:
    '''
    Utility for managing mock methods
    Set it up in your tests setUp(self), and undoAll
    in your tests tearDown(self)
    '''
    def __init__(self):
        self._mocks = []

    def mock(self, module, oldmethod, newmethod):
        '''
        Registers a mock (and mocks).
        module    - Class or module containing the method
        oldmethod - Reference to the method to mock
        newmethod - Reference to the mock method
        '''
        setattr(module, oldmethod.__name__, newmethod)
        self._mocks.append((module,oldmethod))
    
    def reset(self):
        '''
        Un-mocks all mock methods registered throgh this object.
        '''
        for mo,me in self._mocks:
            setattr(mo, me.__name__, me)


class TestCase(Asserts):
    '''
    This is a hack: numpy somehow manages to load this module as "unittest",
    and attempts to import the class TestCase which is never used.
    '''
    def __init__(self,*args):
        self.fail('Should not be instantiated.')
