# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

'''
This module contains functionality that discover upyt unit tests by
searching a python library.
'''

import time
import sys
import logging
import upyt.args
import upyt.discover_impl

def setLogging(logLevel,logInternal,names):

    # No logging handler
    class NullHandler(logging.Handler):
        def emit(self,record):
            pass

    class NameFilter:

        def __init__(self, names):
            self.names = names.split(',')

        def filter(self, record):
            for n in self.names:
                if record.name.startswith(n):
                    return record
            return None

    if logLevel.upper() == 'OFF':
        logging.getLogger('').addHandler(NullHandler())    
    else:
        level = getattr(logging, logLevel.upper())
        format = ':%(levelname)s: - %(name)s :: %(message)s'
        logging.basicConfig(stream=sys.stdout, level=level, format=format)

        for h in logging.root.handlers:
            h.addFilter(NameFilter(names))

        if not logInternal:
            logging.getLogger('upyt').setLevel(logging.CRITICAL + 10)

def systemExit(event):
    '''
    Calls sys.exit(...) with an exit code that matches
    the given event. The event must be one of
    "ok", "usage" or "error".
    '''
    codes = {'ok'   :0, 
             'usage':1, 
             'error':2}

    sys.exit(codes[event])

def createArgsParser():
    opts = 'avh'
    kws={'runAll':bool,
         'verbose':bool,
         'logLevel':str,
         'logInternal':bool,
         'logNames':str,
         'baseDir':str,
         'filePattern':str,
         'testPattern':str,
         'help':None,
         'record':str,
         'replay':str}

    defaults={'runAll':True,
              'verbose':False,
              'logLevel':'off',
              'logInternal':False,
              'logNames':'',
              'filePattern':'*.py',
              'testPattern':'.*',
              'baseDir':'test'}

    help = {}
    help['runAll']  = \
        'Runs all tests, even if some fails.'

    help['verbose'] = \
        'Prints the test name prior to running the test. This is ' + \
        'particularly useful if the test crashes hard, and it is difficult to ' + \
        'understand which test that is failing.'

    help['logLevel'] = \
        'Turns on python\'s built-in logging facility with the given log level. ' + \
        'Logging from the upyt framework is suppressed. To turn this on, you must ' + \
        'specify --logInternal:True in addition.'

    help['logInternal'] = \
        'Turns on logging from the unit test framework code. ' + \
        'Mainly for debugging purposes.'
    
    help['logNames'] = \
        'A comma separated list of the logger names to log from. ' + \
        'For example, specifying --logNames:\'foo.bar,foo.boo\' will filter out ' + \
        'any log message which logger name does not start with either \'foo.bar\' ' + \
        'or \'foo.boo\'.'
 
    help['baseDir'] = \
        'The name of the directory to search for test sources. This _must_ be a ' + \
        'directory that is registered in the PYTHONPATH environment variable.'

    help['filePattern'] = \
        'Regular expression used to specify which file names to include when ' +\
        'looking for files containing tests.\n' + \
        'NOTE: in --filePattern:\'<pattern>\', <pattern> is a regular expression! ' + \
        'This means that, if you want to include a file called \'./../MyTests.py\'' + \
        ', then you can specify --filePattern:\'.*MyTests.*\'.'

    help['testPattern'] = \
        'Regular expression used to specify which test names to include when ' +\
        'looking through files possibly containing tests.\n' + \
        'NOTE: in --testPattern:\'<pattern>\', <pattern> is a regular expression! ' + \
        'This means that, if you want to include a test called\n' + \
        'def testThis(self):\n' +\
        '    ...\n' + \
        'then you could specify --testPattern:\'.*testThis.*\'.'

    help['help'] = \
        'Produces this help text.'

    help['record'] = \
        'Record the names of tests that fails or errs in the specified file. ' + \
        'The failing tests can then be re-run using "--replay", and specifying the ' + \
        'same file name.'

    help['replay'] = \
        'Re-run tests that failed while "--record" was specified. The specified ' + \
        'file name must be the same as that specified to "--record".'
    
    return upyt.args.args(opts=opts, kws=kws, 
                          defaults=defaults, help=help)


if __name__ == "__main__":

    log   = logging.getLogger('upyt.discover.__main__')
    args  = createArgsParser()
    start = time.time()
    try:
        args.parse()
        setLogging(args.logLevel, args.logInternal, args.logNames)
        log.info('Running with ' + str(args))

        if args.help or args.h:
            print(args.usage())
            systemExit('usage')

        if args.baseDir is None:
            print(args.spec())
            systemExit('usage')

        testPattern=args.testPattern
        if args.replay:
            with open(args.replay, 'r') as inf:
                testNames = [line.strip() for line in inf.readlines()]
                
            for i in range(len(testNames)):
                tn = testNames[i][:-1]
                tn = tn.replace('(self) [', '.*')
                testNames[i] = '.*' + tn + '.*'

            testPattern = '|'.join(testNames)
            
        utd   = upyt.discover_impl.UnitTestDiscoverer()
        suite = utd.discoverTestsInPath(baseDir=args.baseDir,
                                        filePattern=args.filePattern,
                                        testPattern=testPattern)

        ok = suite.run(runAll=args.runAll or args.a,
                       startTime=start,
                       verbose=args.verbose or args.v)

        if not ok:
            if args.record:
                with open(args.record, 'w') as outf:
                    for tn in suite.getFailedTestNames():
                        outf.write(tn + '\n')

            systemExit('error')

    except upyt.args.ArgSyntaxError as e:
        print(e)
        print(args.spec())
        systemExit('usage')
