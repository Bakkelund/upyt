# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import upyt.unittest as ut
import sys
import traceback
import time
import logging

def announce(msg):
    print('-'*80)
    print(msg)
    print('-'*80)
        
class ErrorReport:
    def __init__(self, test, msg, stacktrace):
        self._test  = test
        self._msg   = msg
        self._stack = stacktrace

    def __str__(self):
        result  = 'Test: ' + self._test.longstr() + '\n'
        result += self._msg + '\n'
        result += self._stack
        return result
        
class TestSuite:
    
    def __init__(self, tests=[], name='<no name>'):
        self._tests   = tests
        self._name    = name
        self._verbose = False
        self._reports = []
        self._log     = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def append(self, tests):
        self._log.info('Tests appended to suite "' + self._name + '": ' + str(tests))
        self._tests.extend(tests)

    def extend(self, suite):
        self.append(suite._tests)
        
    def run(self, runAll=True, startTime=time.time(), verbose=False):
        self._verbose = verbose
        self._log.info('Test suite "' + self._name + '" running with runAll=' + str(runAll) + \
                           ', verbose=' + str(verbose))
        if len(self._tests) == 0:
            announce('No tests to run.')
            return
        
        announce('Running ' + str(len(self._tests)) +  ' tests.')
        self._prepExecution()
        executor = self._getTestExecutor(runAll)
        for test in self._tests:
            executor(test)

        if not self._verbose:
            print('')
            
        self._report(time.time() - startTime)

        return len(self._reports) == 0

    def getFailedTestNames(self):
        return [rep._test.longstr() for rep in self._reports]

    def _prepExecution(self):
        self._errors   = 0
        self._failures = 0
        self._executed = 0        
        self._goalsMissed   = 0
        self._goalsAchieved = 0

    def _report(self, duration):
        print('-'*80)
        print('Ran %1.0f tests in %1.3f s.' % (self._executed, duration))
            
        print('-'*80)

        if not self._verbose and len(self._reports) > 0:
            print('-'*80)
            print('Failures and Errors:')
            print('-'*80)
            for r in self._reports:
                print(r)
            print('-'*80)


        if self._failures == 0 and self._errors == 0 and self._goalsAchieved == 0 and self._goalsMissed == 0:
            print('SUCCEEDED!!!')
        elif self._goalsAchieved == 0 and self._goalsMissed == 0:
            print('#errors: %1.0f, #failures: %1.0f' % (self._errors, self._failures))
        else:
            print('#errors: %1.0f, #failures: %1.0f, #goals achieved: %1.0f, #goals missed: %1.0f' % 
                  (self._errors, self._failures, self._goalsAchieved, self._goalsMissed))
                        
        print('-'*80)

    def _getTestExecutor(self, runAll):
        if runAll:
            return self._runSingleRobustTest
        else:
            return self._runSingleTest
            
            
    def _runSingleTest(self, test):
        self._printRunMessage(test)
        self._executed += 1
        test.run()        
        self._printSuccessMessage(test)

    def _runSingleRobustTest(self, test):
        try:
            self._runSingleTest(test)
        except ut.GoalAchieved as a:
            self._printGoalAchievedMessage(test, a)
            self._goalsAchieved += 1
        except ut.GoalError as e:
            self._printGoalErrorMessage(test, e)
            self._goalsMissed += 1
        except ut.AssertException as e:
            self._printFailureMessage(test, e)
            self._failures += 1
        except:
            self._printErrorMessage(test)
            self._errors += 1

    def _printRunMessage(self, test):
        if self._verbose:
            sys.stdout.write(test.longstr())

    def _printSuccessMessage(self, test):
        if self._verbose:
            sys.stdout.write(' ')
        sys.stdout.write('.')
        if self._verbose:
            print('')
            
    def _printErrorMessage(self, test):
        if self._verbose:
            print('ERROR:')
            print(traceback.format_exc())
        else:
            sys.stdout.write('E')
            self._reports.append(ErrorReport(msg='ERROR',
                                             test=test,
                                             stacktrace=traceback.format_exc()))


    def _printFailureMessage(self, test, e):
        if self._verbose:
            print('FAILURE:', e)
            print(traceback.format_exc())
        else:
            sys.stdout.write('F')
            self._reports.append(ErrorReport(msg='FAILURE:' + str(e),
                                             test=test,
                                             stacktrace=traceback.format_exc()))


    def _printGoalErrorMessage(self, test, e):
        if self._verbose:
            print('GOAL ACHIEVEMENT FAILED:', e)
            print(traceback.format_exc())
        else:
            sys.stdout.write('[g]')
            self._reports.append(ErrorReport(msg='GOAL NOT ACHIEVED: ' + str(test),
                                             test=test,
                                             stacktrace=''))

    def _printGoalAchievedMessage(self, test, a):
        if self._verbose:
            print('GOAL ACHIEVED!!!!:', str(test))
        else:
            sys.stdout.write('[G!]')
            self._reports.append(ErrorReport(msg='GOAL ACHIEVED!!! -- ' + str(test),
                                             test=test,
                                             stacktrace=''))
            
    def __iter__(self):
        return iter(self._tests)

class Test:
    def __init__(self, fileName, testFunction):
        self._test = testFunction
        self._file = fileName

    def run(self):
        self._test.__self__.setUp()
        try:
            self._test()
        finally:
            self._test.__self__.tearDown()

    def __str__(self):
        return '%s.%s(self)' % (self._test.__self__.__class__.__name__, 
                                self._test.__name__)

    def longstr(self):
        return '%s [file:%s]' % (str(self), self._file)
