# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
'''
This module contains functionality that discover upyt unit tests by
searching a python library.
'''

import inspect
import re
import os
import os.path
import glob
import logging
import upyt.unittest
import upyt.args
import upyt.fixture

class UnitTestDiscoverer:

    def __init__(self):
        self._log = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def discoverTestsInPath(self, baseDir, filePattern, testPattern):
        tests  = []
        files  = self._findFilesInPath(baseDir, filePattern)
        testRe = re.compile(testPattern)
        for f in files:
            mod = self._getModuleFromFile(f, baseDir)
            clz = self._locateClassesFromModule(mod)
            tst = self._locateTestsFromClasses(clz)
            for t in tst:
                test = upyt.fixture.Test(fileName=f, testFunction=t)
                if testRe.match(test.longstr()) is not None:
                    tests.append(test)
                
        suiteName = '{baseDir:%s,files:%s,tests:%s}' % (baseDir,filePattern,testPattern)
        return upyt.fixture.TestSuite(tests=tests, name=suiteName)
        
    def _locateTestsFromClasses(self, classList):
        result = []
        for c in classList:
            if issubclass(c, upyt.unittest.UnitTest):
                self._log.debug('Test class "' + str(c) + '" in test module.')
                t = c()
                tests = t.getTestMethods()
                self._log.debug('Located test methods ' + str(tests) + 'from class ' + str(c))    
                result.extend(tests)
            else:
                self._log.debug('Non test class "' + str(c) + '" in test module.')

        return result
            

    def _locateClassesFromModule(self, module):
        attrs  = [getattr(module, d) for d in dir(module)]
        result = [a for a in attrs if inspect.isclass(a)]
        self._log.info('Located test classes ' + str(result) + ' in module ' + str(module))
        return result

    def _getModuleFromFile(self, fname, baseDir):
        m    = re.match('^(?P<path>.*?)(\.py)$', fname)
        path = m.group('path')
        modn = self._replaceFileSepWithDot(path)
        # We must remove the source directory from the module name
        base = self._replaceFileSepWithDot(baseDir)
        modn = modn[len(base)+1:]

        self._log.debug('Loading module "' + modn + '" from file "' + fname + '"')
        modu = self._loadModule(modn)
        self._log.debug('Loaded module "' + str(modu) + '"')
        
        return modu

    def _replaceFileSepWithDot(self, path):
        result = path.replace('\\', '.')
        return result.replace('/', '.')

    def _loadModule(self, modName):        
        tail = modName
        name = modName.replace('.','_')
        m    = re.match(r'(?P<head>.+)\.(?P<tail>.+)', modName)
        if not m is None:
            head = m.group('head')
            tail = m.group('tail')
            stmt = 'from ' + head + ' import ' + tail + ' as ' + name
            self._log.debug('Executing: "' + stmt + '"')
            exec(stmt)
        else:
            stmt = 'import ' + modName + ' as ' + name
            self._log.debug('Executing: "' + stmt + '"')
            exec(stmt)

        return locals()[name]

    def _findFilesInPath(self, baseDir, globPattern):
        result = []
        walker = DirWalker(baseDir)
        sep    = os.path.sep
        for d in walker:
            tests = glob.glob(d + sep + globPattern)
            result.extend(tests)

        self._log.info('Discovered files: ' + str(result))
        return result


class DirWalker:
    def __init__(self, startDir):
        if not os.path.isdir(startDir):
            raise Exception('The start location "' + startDir + '" is not a directory.')
        self._dirList = []        
        self._loadDirs(startDir)

    def _loadDirs(self, startDir):
        sep     = os.path.sep
        subdirs = [startDir + sep + d for d in os.listdir(startDir) \
                   if os.path.isdir(startDir + sep + d)]
        for newDir in subdirs:
            self._dirList.append(newDir)
            self._loadDirs(newDir)
            
    def __iter__(self):
        return iter(self._dirList)

