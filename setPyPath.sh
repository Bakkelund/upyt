# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2020 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

OSNAME=`uname -s`

if [[ $OSNAME == CYGWIN* ]]; then
    echo "Cygwin detected."
    P=`cygpath -a -w $PWD/src`
    PP=${P//\\/\\\\}
    T=`cygpath -a -w $PWD/test`
    TP=${T//\\/\\\\}
    export PYTHONPATH=${PP}';'${TP}
else
    echo "Assuming $OSNAME has Unix-style path layout."
    export PYTHONPATH=${PWD}/src':'${PWD}/test
fi

echo "PYTHONPATH=$PYTHONPATH"
